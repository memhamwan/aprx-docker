# First, make the darn thing
FROM gcc:9 AS gcc

COPY aprx /tmp/aprx
WORKDIR /tmp/aprx

RUN ./configure --with-pthread --with-embedded
RUN make clean
RUN make 
RUN make install

# Then, setup alpine as our runtime environment with gcompat and copy the install files into alpine
FROM alpine:latest
RUN apk add gcompat
COPY --from=gcc /sbin/aprx /usr/sbin/aprx
COPY --from=gcc /etc/aprx.conf /etc/aprx.conf

CMD [ "aprx" ]
