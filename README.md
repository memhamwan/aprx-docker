# APRX Docker

This is a wrapping project that simply adds a dockerfile and publishes a docker image for running aprx.

## How do the APRX sources get pulled in?

Using a git submodule to their [parent repository](https://github.com/PhirePhly/aprx.git). Please note that they retain their own license to their work.

If you've never used a submodule before, [read the docs](https://git-scm.com/book/en/v2/Git-Tools-Submodules) for a reasonable introduction. The tl;dr is to `cd aprx && git submodule init && git submodule update`.
